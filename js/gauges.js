Object.extend = function(destination, source) {
    for(var property in source) {
        if(source.hasOwnProperty(property)) {
            if( destination[property] != null && typeof destination[property] == 'object') {
              destination[property] = Object.extend(destination[property], source[property])
            } else {
              destination[property] = source[property];
            }
        }
    }
    return destination;
};

function IADAGauge(options) {
    this.options = Object.extend({
    container: 'gauge',
    canvas: null,
    position: {top: null, left: null},
    margins: {top: 10, left: 10, bottom: 10, right: 10},
    size: {height: 200, width: 200},
    scale: {min: -10, max: 40, step: 1, bigStep: 5, scaleMiddleAt: 'bottom'},
    scaleColoring: [
      {from: -10, to: 0, color: '#4183C4'},
      {from: 20, to: 30, color: '#FFDB14'},
      {from: 30, to: 40, color: '#DA3838'},
    ],
    label: 'Temp (�C)',
    labelPosition: 'top', // top or bottom
    degreeSpace: 250,
    scaleIndicator: {size: 10, margin: 10, fontSize: 12},
    needle: {initialValue: 0, pointRadius: 5},
    }, options || {});

  /**
  this.options = Object.extend({
    container: 'gauge',
    canvas: null,
    position: {top: null, left: null},
    margins: {top: 10, left: 10, bottom: 10, right: 10},
    size: {height: 200, width: 200},
    scale: {min: 0, max: 60, step: 1, bigStep: 5, scaleMiddleAt: 'bottom'},
    scaleColoring: [],
    label: '',
    labelPosition: 'bottom', // top or bottom
    degreeSpace: 360,
    scaleIndicator: {size: 5, margin: 5, fontSize: 12},
    needle: {initialValue: 0, pointRadius: 5},
  }, options || {});

  */
  console.debug(this.options);

  this.needleValue = this.options.needle.initialValue;
  this.render();
}

IADAGauge.prototype.setWidth = function(newWidth) {
  this.options.size.width = newWidth;
}

IADAGauge.prototype.setHeight = function(newHeight) {
  this.options.size.height = newHeight;
}

IADAGauge.prototype.getCanvas = function() {
  return this.canvas;
}

IADAGauge.prototype.render = function() {
  this.createCanvas();
  this.renderBase();
  this.renderBaseLabel();
  this.renderColorScale();
  this.renderScale();
  this.renderNeedleStopPoints();
  this.renderTriangularNeedle();
  //this.renderSimpleNeedle();
  this.renderMiddleDot();
}

IADAGauge.prototype.createCanvas = function() {
  if(this.options.canvas != null && this.options.canvas.constructor != null && this.options.canvas.constructor.prototype == Raphael.prototype) {
    // We already have a canvas
    this.canvas = this.options.canvas;
  }
  else {
    this.canvas = Raphael(this.options.container, this.options.size.width + this.options.margins.left + this.options.margins.right, this.options.size.height + this.options.margins.top + this.options.margins.bottom);
  }
}

IADAGauge.prototype.renderBase = function() {
  // Big circle
  var circle = this.canvas.circle(this.getMiddle().x, this.getMiddle().y, this.getRadius());
  this.addShadow(circle);
}

IADAGauge.prototype.renderBaseLabel = function() {
  this.canvas.text(this.getMiddle().x, (this.options.labelPosition == 'top') ? (this.getMiddle().y - (this.getRadius() / 2)) : (this.getMiddle().y + (this.getRadius() / 2)), this.options.label).attr({'font-size': 16});
}

IADAGauge.prototype.renderMiddleDot = function() {
  var dot = this.canvas.circle(this.getMiddle().x, this.getMiddle().y, this.options.needle.pointRadius);
  dot.attr({stroke: 'none', fill: '#555'});
}

IADAGauge.prototype.addShadow = function(element) {
  element.glow({width: 1, opacity: 0.2, offsety: 0.5, offsetx: 1});
}

IADAGauge.prototype.getRadius = function() {
  return Math.min(this.options.size.width, this.options.size.height) / 2;
}

IADAGauge.prototype.renderScale = function() {
  // Calculate number of steps
  this.options.degreeSpace = Math.min(this.options.degreeSpace, 360);

  var nrSteps = (this.options.scale.max - this.options.scale.min) / this.options.scale.step;
  var degreePerStep = this.options.degreeSpace / nrSteps;
  var degreeStart = -this.scaleStart();

  // Almost whole cicle is used for scale, so no scale label for last step.
  if(this.options.degreeSpace >= 355) {
    nrSteps -= 1;
  }

  itemValue = this.options.scale.min;
  for(var i = degreeStart; i <= ((nrSteps * degreePerStep) + degreeStart); i += degreePerStep) {
    this.renderScaleIndicator(i, itemValue)
    itemValue += this.options.scale.step;
  }
}

IADAGauge.prototype.renderScaleIndicator = function(i, itemValue) {
  // Determine line length
  bigStep = (itemValue % this.options.scale.bigStep == 0)
  // Draw line
  radius = this.getRadius() - this.options.scaleIndicator.margin;
  lineLength = this.options.scaleIndicator.size * (bigStep ? 1.5 : 1);
  this.angleToLine(i, radius, lineLength);
  if(bigStep) {
    // Draw text
    textPoint = this.angleToPoint(i, (radius - lineLength - 5 - (this.options.scaleIndicator.fontSize / 2)));
    this.canvas.text(textPoint.x, textPoint.y, itemValue).attr({'font-size': this.options.scaleIndicator.fontSize});
  }
}

IADAGauge.prototype.renderColorScale = function() {
  radius = this.getRadius() - this.options.scaleIndicator.size / 2 - this.options.scaleIndicator.margin;
  for(var key in this.options.scaleColoring) {
    curitem = this.options.scaleColoring[key];
    from = this.angleToPoint(this.needleDeg(curitem.from), radius).toPath();
    to = this.angleToPoint(this.needleDeg(curitem.to), radius).toPath();
    this.canvas.path('M' + from + " " + 'A' + radius.toString() + " " + radius.toString() + " 0 0 1 " + to).attr({'stroke': curitem.color, 'stroke-width': this.options.scaleIndicator.size});
  }
}

IADAGauge.prototype.renderNeedleStopPoints = function() {
  if(this.options.degreeSpace < 355) {
    minstop = this.angleToPoint(this.needleDeg(this.options.scale.min - 2), this.getRadius() / 2);
    this.canvas.circle(minstop.x, minstop.y, 2).attr({stroke: 'none', fill: '#555'});
    maxstop = this.angleToPoint(this.needleDeg(this.options.scale.max + 2), this.getRadius() / 2);
    this.canvas.circle(maxstop.x, maxstop.y, 2).attr({stroke: 'none', fill: '#555'});
  } else {
    middlestop = this.angleToPoint(0, this.getRadius() / 2);
    this.canvas.circle(middlestop.x, middlestop.y, 2).attr({stroke: 'none', fill: '#555'});
  }
}

IADAGauge.prototype.scaleStart = function() {
  switch (this.options.scale.scaleMiddleAt) {
    case 'bottom':
      start = 270 + this.options.degreeSpace / 2;
      break;
    case 'left':
      start = 180 + this.options.degreeSpace / 2;
      break;
    case 'right':
      start = this.options.degreeSpace / 2;
      break;
    default:
      start = 90 + this.options.degreeSpace / 2;
      break;
    }
    return start % 360;
}

IADAGauge.prototype.writeLine = function(from, to) {
  this.canvas.path('M' + from.x.toString() + " " + from.y.toString() + 'L' + to.x.toString() + " " + to.y.toString());
}

IADAGauge.prototype.angleToPoint = function(angle, radius, fromMiddle = true) {
  point = Point.polar(angle, radius);
  if(fromMiddle) {
    point = point.add(this.getMiddle());
  }
  return point
}

IADAGauge.prototype.angleToLine = function(angle, radius, length) {
  lineStartPoint = this.angleToPoint(angle, (this.getRadius() - this.options.scaleIndicator.margin));
  lineEndPoint = lineStartPoint.sub(this.angleToPoint(angle, length, false));
  this.writeLine(lineStartPoint, lineEndPoint);
}

IADAGauge.prototype.getMiddle = function() {
  if(this.options.position.top == null) {
    this.options.position.top = this.options.size.height / 2;
  }
  if(this.options.position.left == null) {
    this.options.position.left = this.options.size.width / 2;
  }
  return new Point((this.options.margins.left + this.options.position.left), (this.options.margins.top + this.options.position.top));
}

IADAGauge.prototype.needleInitPoint = function() {
  return this.angleToPoint(0, (this.getRadius() - this.options.scaleIndicator.margin - (this.options.scaleIndicator.size / 2)));
}

IADAGauge.prototype.needleDeg = function(value) {
    return ((value - this.options.scale.min) / (this.options.scale.max - this.options.scale.min) * this.options.degreeSpace) - this.scaleStart();
}

IADAGauge.prototype.update = function(newValue) {
  if(typeof newValue == 'undefined') {
    newValue = 0;
  } else if (newValue > this.options.scale.max + 1) {
      newValue = this.options.scale.max + 1;
  } else if (newValue < this.options.scale.min - 1) {
    newValue = this.options.scale.min - 1
  }
  var newNeedleDeg = this.needleDeg(newValue);
  this.needleValue = newValue;
  this.needle.animate({transform: 'r' + newNeedleDeg.toString() + ',' + this.getMiddle().x.toString() + ',' + this.getMiddle().y.toString()}, 500);
}

IADAGauge.prototype.renderSimpleNeedle = function() {
  this.needle = this.canvas.path('M' + this.getMiddle().toPath() + 'L' + this.needleInitPoint().toPath());
  // To initial value
  this.needle.transform('r' + (this.needleDeg(this.needleValue)).toString() + ',' + this.getMiddle().x.toString() + ',' + this.getMiddle().y.toString());
  this.needle.attr({stroke: '#3E78FD'});
}

IADAGauge.prototype.renderTriangularNeedle = function() {
  this.needle = this.canvas.path(
    'M' + this.needleInitPoint().toPath() +
    'L' + this.angleToPoint(90, this.options.needle.pointRadius).toPath() +
    'L' + this.angleToPoint(135, 3 * this.options.needle.pointRadius).toPath() +
    'L' + this.angleToPoint(180, 5 * this.options.needle.pointRadius).toPath() +
    'L' + this.angleToPoint(225, 3 * this.options.needle.pointRadius).toPath() +
    'L' + this.angleToPoint(270, this.options.needle.pointRadius).toPath() +
    'z'
  );
  // To initial value
  this.needle.transform('r' + (this.needleDeg(this.needleValue)).toString() + ',' + this.getMiddle().x.toString() + ',' + this.getMiddle().y.toString());
  this.needle.attr({fill: '#3E78FD', stroke: 'none'});
}